var RescanReason = Object.freeze({
  PERIODIC: 0,
  URL_CHANGE: 1,
  MANUAL: 2
});

export default RescanReason;
