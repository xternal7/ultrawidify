var EdgeDetectQuality = Object.freeze({
  FAST: 0,
  IMPROVED: 1
});

export default EdgeDetectQuality;
