var EdgeStatus = Object.freeze({
  AR_UNKNOWN: 0,
  AR_KNOWN: 1,
});

export default EdgeStatus;
