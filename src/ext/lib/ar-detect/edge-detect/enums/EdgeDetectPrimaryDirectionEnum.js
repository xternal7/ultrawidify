var EdgeDetectPrimaryDirection = Object.freeze({
  VERTICAL: 0,
  HORIZONTAL: 1
});

export default EdgeDetectPrimaryDirection;
