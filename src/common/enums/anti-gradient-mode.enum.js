var AntiGradientMode = Object.freeze({
  Disabled: 0,
  Lax: 1,
  Strict: 2
});

export default AntiGradientMode;
