var AspectRatio = Object.freeze({
  Initial: -1,
  Reset: 0,
  Automatic: 1,
  FitWidth: 2,
  FitHeight: 3,
  Fixed: 4,
});

export default AspectRatio;
