var Stretch = Object.freeze({
  NoStretch: 0,
  Basic: 1,
  Hybrid: 2,
  Conditional: 3,
  Default: -1
});

export default Stretch;
