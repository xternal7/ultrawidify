var VideoAlignment = Object.freeze({
  Left: 0,
  Center: 1,
  Right: 2,
  Default: -1
});

export default VideoAlignment;
